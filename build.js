const s3 = require('s3')
const path = require('path')
const ora = require('ora')
const PromiseQueue = require('promise-queue')

const buildSite = require('./template/build/build.js')

const workerQueue = new PromiseQueue(1, 1000)

const S3_ACCESS_KEY = process.env.S3_ACCESS_KEY
const S3_SECRET = process.env.S3_SECRET
const S3_BUCKET = process.env.S3_BUCKET

/*
 * Create an instance of S3 client.
 * it has pool of connections for better performance
 */
const s3Client = s3.createClient({
  s3RetryCount: 1,
  s3Options: {
    accessKeyId: S3_ACCESS_KEY,
    secretAccessKey: S3_SECRET,
    region: 'us-west-2'
  }
})

/*
 * Uploads built site files to S3 storage
 */
const pushWebsiteToS3 = (siteDir, domain) =>
  new Promise((resolve, reject) => {
    const progressText = (percent) =>
      `Uploading site to S3://${domain} # ${(percent || 0).toFixed(2)}%`

    const spinner = ora(progressText()).start()

    const uploader = s3Client.uploadDir({
      localDir: siteDir,

      // default false, whether to remove s3 objects
      // that have no corresponding local file.
      deleteRemoved: true,

      s3Params: {
        Bucket: S3_BUCKET,
        Prefix: domain
      }
    })

    uploader.on('error', error => {
      reject(error)
      spinner.stopAndPersist('err')
    })

    uploader.on('end', () => {
      resolve()
      spinner.stopAndPersist('ok')
    })

    uploader.on('progress', () => {
      const percent = (uploader.progressAmount / uploader.progressTotal) * 100
      spinner.text = progressText(percent)
    })
  })

/*
 * Builds a static site using provided payload
 */
const buildRoutine = (siteConfig) => {
  let spinner = ora('building using Webpack').start()

  return buildSite(siteConfig.data, { silent: true })
  .then(status => {
    const sourceDir = path.join(__dirname, 'template', 'dist')
    spinner.stopAndPersist('ok')
    return pushWebsiteToS3(sourceDir, siteConfig.domain)
  })
}

const initiateBuild = (config) =>
  workerQueue.add(() => buildRoutine(config))

/*
 * USAGE
 */

module.exports = initiateBuild
