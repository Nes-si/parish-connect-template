/* eslint-disable */

module.exports = {
  "Parish": {
    "name": "Example Parish",
    "street": "Example St.",
    "city": "New-York",
    "state": "California",
    "country": "USA",
    "zip": "123456",
    "phonePrimary": "0987654321",
    "email": "example@examp.le",
    "languageDefault": {
      "name": "English",
      "encodedValue": "en",
      "image": null
    },
    "imageLogo": null,
    "facebook": "http://www.facebook.com",
    "twitter": "http://www.twitter.com",
    "instagram": "http://instagram.com",
    "youtube": "http://youtube.com",
    "imageMission": null,
    "imageAbout": null,
    "domain": "http://www.reg.ru"
  },
  "About": {
    "parish": {
      "name": "Example Parish",
      "street": "Example St.",
      "city": "New-York",
      "state": "California",
      "country": "USA",
      "zip": "123456",
      "phonePrimary": "0987654321",
      "email": "example@examp.le",
      "languageDefault": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "imageLogo": null,
      "facebook": "http://www.facebook.com",
      "twitter": "http://www.twitter.com",
      "instagram": "http://instagram.com",
      "youtube": "http://youtube.com",
      "imageMission": null,
      "imageAbout": null,
      "domain": "http://www.reg.ru"
    },
    "language": {
      "name": "English",
      "encodedValue": "en",
      "image": null
    },
    "description": "We are amazing!",
    "detail": "<p>We are amazing! Let's come together!</p>"
  },
  "MissionStatement": {
    "parish": {
      "name": "Example Parish",
      "street": "Example St.",
      "city": "New-York",
      "state": "California",
      "country": "USA",
      "zip": "123456",
      "phonePrimary": "0987654321",
      "email": "example@examp.le",
      "languageDefault": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "imageLogo": null,
      "facebook": "http://www.facebook.com",
      "twitter": "http://www.twitter.com",
      "instagram": "http://instagram.com",
      "youtube": "http://youtube.com",
      "imageMission": null,
      "imageAbout": null,
      "domain": "http://www.reg.ru"
    },
    "language": {
      "name": "English",
      "encodedValue": "en",
      "image": null
    },
    "detail": "We are wonderful!"
  },
  "ParishAdministration": {
    "parish": {
      "name": "Example Parish",
      "street": "Example St.",
      "city": "New-York",
      "state": "California",
      "country": "USA",
      "zip": "123456",
      "phonePrimary": "0987654321",
      "email": "example@examp.le",
      "languageDefault": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "imageLogo": null,
      "facebook": "http://www.facebook.com",
      "twitter": "http://www.twitter.com",
      "instagram": "http://instagram.com",
      "youtube": "http://youtube.com",
      "imageMission": null,
      "imageAbout": null,
      "domain": "http://www.reg.ru"
    },
    "position": {
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "name": "precentor"
    },
    "title": {
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "letters": "Minister"
    },
    "nameFirst": "John",
    "nameLast": "Doe",
    "image": null
  },
  "ParishAdminBio": {
    "parishAdministration": {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "position": {
        "language": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "name": "precentor"
      },
      "title": {
        "language": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "letters": "Minister"
      },
      "nameFirst": "John",
      "nameLast": "Doe",
      "image": null
    },
    "language": {
      "name": "English",
      "encodedValue": "en",
      "image": null
    },
    "bio": "I am the best!"
  },
  "Rectory": {
    "parish": {
      "name": "Example Parish",
      "street": "Example St.",
      "city": "New-York",
      "state": "California",
      "country": "USA",
      "zip": "123456",
      "phonePrimary": "0987654321",
      "email": "example@examp.le",
      "languageDefault": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "imageLogo": null,
      "facebook": "http://www.facebook.com",
      "twitter": "http://www.twitter.com",
      "instagram": "http://instagram.com",
      "youtube": "http://youtube.com",
      "imageMission": null,
      "imageAbout": null,
      "domain": "http://www.reg.ru"
    },
    "title": {
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "letters": "Minister"
    },
    "nameFirst": "Jane",
    "nameLast": "Smith",
    "street": "First str.",
    "city": "Los-Angeles",
    "state": "Washington",
    "country": "USA",
    "phone": "04874939",
    "email": "rectory@examp.le"
  },
  "School": {
    "parish": {
      "name": "Example Parish",
      "street": "Example St.",
      "city": "New-York",
      "state": "California",
      "country": "USA",
      "zip": "123456",
      "phonePrimary": "0987654321",
      "email": "example@examp.le",
      "languageDefault": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "imageLogo": null,
      "facebook": "http://www.facebook.com",
      "twitter": "http://www.twitter.com",
      "instagram": "http://instagram.com",
      "youtube": "http://youtube.com",
      "imageMission": null,
      "imageAbout": null,
      "domain": "http://www.reg.ru"
    },
    "type": "HIGH",
    "title": {
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "letters": "Minister"
    },
    "name": "St. School",
    "contactNameFirst": "Joseph",
    "contactNameLast": "Green",
    "street": "First str.",
    "city": "Los-Angeles",
    "state": "Washington",
    "country": "USA",
    "phone": "04874939",
    "email": "rectory@examp.le"
  },
  "SliderMedia": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "position": 1,
      "image": null
    }
  ],
  "MassTime": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "massType": {
        "language": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "name": "Feast Day"
      },
      "timeStart": 949402800000,
      "timeEnd": 949406400000,
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "sunday": false,
      "monday": true,
      "tuesday": false,
      "wednesday": true,
      "thursday": false,
      "friday": false,
      "saturday": true
    }
  ],
  "ConfessionTime": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "timeStart": 949420800000,
      "timeEnd": 949424400000,
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "sunday": false,
      "monday": true,
      "tuesday": false,
      "wednesday": false,
      "thursday": false,
      "friday": true,
      "saturday": true
    }
  ],
  "OtherTime": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "type": {
        "language": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "name": "Other time 2"
      },
      "timeStart": 949399200000,
      "timeEnd": 949402800000,
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "sunday": false,
      "monday": false,
      "tuesday": false,
      "wednesday": false,
      "thursday": false,
      "friday": false,
      "saturday": true
    }
  ],
  "Sacrament": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "name": "Sacrament1",
      "detail": "<p>\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a <b>complete</b> account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or <u>pursues</u> or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p>"
    },
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "name": "Sacrament Title 2",
      "detail": "\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. <b>Nam</b> libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"<p></p>"
    }
  ],
  "Ministry": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "name": "Ministry1",
      "detail": "<p></p>"
    }
  ],
  "OtherProgram": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "language": {
        "name": "English",
        "encodedValue": "en",
        "image": null
      },
      "name": "Other Program 1",
      "detail": "We are not such as all"
    }
  ],
  "Staff": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "position": {
        "language": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "name": "precentor"
      },
      "nameFirst": "Josh",
      "nameLast": "Brown",
      "photo": null,
      "phone": "04874939",
      "email": "staff@examp.le"
    }
  ],
  "Bulletin": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "name": "Meetup",
      "file": null
    }
  ],
  "Announcement": [
    {
      "parish": {
        "name": "Example Parish",
        "street": "Example St.",
        "city": "New-York",
        "state": "California",
        "country": "USA",
        "zip": "123456",
        "phonePrimary": "0987654321",
        "email": "example@examp.le",
        "languageDefault": {
          "name": "English",
          "encodedValue": "en",
          "image": null
        },
        "imageLogo": null,
        "facebook": "http://www.facebook.com",
        "twitter": "http://www.twitter.com",
        "instagram": "http://instagram.com",
        "youtube": "http://youtube.com",
        "imageMission": null,
        "imageAbout": null,
        "domain": "http://www.reg.ru"
      },
      "title": "Meetup",
      "name": "Meetup is here",
      "description": "JS Meetup",
      "content": "<p>JS Meetup is here!</p>",
      "date": 1481414400000
    }
  ]
}
