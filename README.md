## Building images
```
docker build -t yarnynode -f Dockerfile-node .
```

## Proxy server and bucker configuration
Make sure static hosting is enabled for the bucket. But it won't work until you
add following policy to the bucket:

```js
{
  "Version":"2012-10-17",
  "Statement":[{
  "Sid":"PublicReadGetObject",
        "Effect":"Allow",
    "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::YOUR_BUCKET_NAME/*"
      ]
    }
  ]
}
```

## Running proxy server
```
# dev
cd proxy && docker run --rm --name parish-proxy -p 1338:80 -v `pwd`/nginx.conf:/etc/nginx/nginx.conf:ro nginx

# production
cd proxy && docker run -d --name parish-proxy -p 80:80 -v `pwd`/nginx.conf:/etc/nginx/nginx.conf:ro nginx
```
