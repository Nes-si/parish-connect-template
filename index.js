const express = require('express')
const ParseServer = require('parse-server').ParseServer
const S3Adapter = require('parse-server').S3Adapter;
const path = require('path')
const env = require('node-env-file')
const Parse = require('parse/node')
const bodyParser = require('body-parser')
const Mailgun = require('mailgun-js')

env(path.join(__dirname, '/.env'))


// Settings
const port = process.env.PORT || 1337
const databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI || "mongodb://localhost:27017/parishconnect"
const serverUrl = `http://localhost:${port}/parse`
const masterKey = process.env.MASTER_KEY
const appId = process.env.APP_ID || 'parish-connect'

const S3_ACCESS_KEY = process.env.S3_ACCESS_KEY
const S3_SECRET = process.env.S3_SECRET
const S3_BUCKET = process.env.S3_BUCKET
const MAILGUN_KEY = process.env.MAILGUN_KEY

Parse.initialize(appId)
Parse.serverURL = serverUrl


let api = new ParseServer({
  databaseURI: databaseUri,
  cloud: path.join(__dirname, '/cloud/main.js'),
  appId: appId,
  masterKey: masterKey,
  serverURL: serverUrl,
  filesAdapter: new S3Adapter(
  S3_ACCESS_KEY,
  S3_SECRET,
  "parish-assets",
   {
     directAccess: true,
     region: 'us-west-2'
   }
 ),
  liveQuery: {
    classNames: [] // List of classes to support for query subscriptions
  }
})

let app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/',
  express.static(path.join(__dirname, '..', '/parish-cms')))

app.get('/api/compile', (req, res) => {

  Parse.Cloud.run('localbuild')
    .then(() => res.json({status: 'ok' }))
    .catch((error) => res.json({ status: JSON.stringify(error)}))
})


//CORS HEADERS(ONLY IN DEVELOPMENT)
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.post('/api/send_mail', (req, res) => {

  const apiKey = MAILGUN_KEY
  const domain = 'parish.host'
  const fromWho = req.body.email
  const mailgun = new Mailgun({apiKey, domain})
  const adminEmail = 'nemo48@yandex.ru'
  console.log(req.body)
  const data = {
    from: fromWho,
    to: adminEmail,
    subject: 'Hello from Mailgun',
    html: `<p>Full name: ${req.body.fullName}</p>
      <p>From domain: ${req.body.domain}</p>
      <p>website: ${req.body.website}</p>
      <p>Phone number: ${req.body.phoneNumber}</p>
      <h3>Message</h3>
      <p>${req.body.message}</p>`
  }

  mailgun.messages().send(data, (err, body) => {
    if (err) {
      res.json(err)
    } else {
      res.json(body)
    }
  })
})

app.get('/api/migrate', (req, res) => {

  Parse.Cloud.run('migrateModels')
    .then(() => res.json({status: 'ok' }))
    .catch((error) => res.json({ status: JSON.stringify(error)}))
})

app.get('/api/deploy', (req, res) => {
  const domain = req.query.domain || 'pepe.parish.host'

  Parse.Cloud.run('deploy', { domain: domain })
    .then(() => res.json({ domain, status: 'ok' }))
    .catch((error) => res.json({ status:JSON.stringify(error) }))
})


// Serve the Parse API on the /parse URL prefix
let mountPath = process.env.PARSE_MOUNT || '/parse'
app.use(mountPath, api)

let httpServer = require('http').createServer(app)
httpServer.listen(port, () => {
  console.log('parse-server running on port ' + port + '.')
})

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer)
