FROM yarnynode

RUN mkdir parse
ADD . /parse
WORKDIR /parse

RUN NODE_ENV=production yarn --no-progress --verbose --production

ENV PORT 80
EXPOSE 80

CMD [ "npm", "start" ]
