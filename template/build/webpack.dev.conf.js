const webpack = require('webpack')
const path = require('path')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const utils = require('./utils')

const makeBaseConfig = require('./webpack.base.conf')
const baseWebpackConfig = makeBaseConfig()

const projectRoot = path.resolve(__dirname, '../')

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(name => {
  baseWebpackConfig.entry[name] = [projectRoot + '/build/dev-client']
    .concat(baseWebpackConfig.entry[name])
})

module.exports = () => {
  return merge(baseWebpackConfig, {
    module: {
      loaders: utils.styleLoaders({sourceMap: false})
    },
    devtool: '#eval-source-map',
    plugins: [
      new HtmlWebpackPlugin({
        template: path.join(projectRoot, '/src/index.pug'),
        inject: true,
        filename: 'index.html',
        chunksSortMode: 'dependency'
      }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin()
    ]
  })
}
