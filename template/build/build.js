const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')

const { rm, mkdir } = require('shelljs')

const ExtendedDefinePlugin = require('extended-define-webpack-plugin')
const PrerenderSpaPlugin = require('prerender-spa-plugin')

const makeProdConfig = require('./webpack.prod.conf')

const build = (data, options = {}) => {
  const destPath = path.resolve(__dirname, '../dist')

  rm('-rf', destPath)
  mkdir('-p', destPath)

  const webpackConfig = makeProdConfig()

  const eng = false;

  let finalConfig = merge(webpackConfig, {
    plugins: [
      new ExtendedDefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production' || 'development'),
        DATA: data
      }),
      new PrerenderSpaPlugin(
        path.join(__dirname, '../dist'),
        // List of routes to prerender
        [


          eng ? '/'             :  '/',
          eng ? '/bulletins'    :  '/boletines',
          eng ? '/ministries'   :  '/ministerios',
          eng ? '/programs'     :  '/programas',
          eng ? '/announcements':  '/anuncios',
          eng ? '/about'        :  '/acerca-de',
          eng ? '/rectory'      :  '/casa-del-parroco',
          eng ? '/sacraments'   :  '/sacramentos',
          eng ? '/staff'        :  '/personal',
          eng ? '/worship'      :  '/horarios',
          eng ? '/calendar'     :  '/calendario',
          eng ? '/contact'      :  '/contacto',
          ...(Object.keys(data.Event || {}).map(e => eng ? `/calendar/events/${e}` : `/calendario/eventos/${e}`) || [])
        ]
        ,
        {
          captureAfterTime: 5000,
          maxAttempts: 10,
          phantomOptions: '--disk-cache=true',
          phantomPageSettings: {loadImages: true}
        }
      )
    ]
  })

  return new Promise((resolve, reject) => {
    webpack(finalConfig, (err, stats) => {
      if (err) {
        reject(err)
      }

      !options.silent && process.stdout.write(stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false
      }) + '\n')

      resolve('Built!')
    })
  })
}

module.exports = build
