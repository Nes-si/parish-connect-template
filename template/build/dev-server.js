const express = require('express')
const webpack = require('webpack')
const merge = require('webpack-merge')
const Parse = require('parse/node')

const ExtendedDefinePlugin = require('extended-define-webpack-plugin')
const makeDevConfig = require('./webpack.dev.conf')

let port = process.env.PORT || 8080

function createDevServer (data) {
  let app = express()

  const webpackConfig = makeDevConfig()

  let finalConfig = merge(webpackConfig, {
    plugins: [
      new ExtendedDefinePlugin({
        DATA: data,
        'process.env.NODE_ENV': 'production',
      })
    ]
  })

  let compiler = webpack(finalConfig)

  // handle fallback for HTML5 history API
  app.use(require('connect-history-api-fallback')())

  let devMiddleware = require('webpack-dev-middleware')(compiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: {
      colors: true,
      chunks: false
    }
  })

  app.use(devMiddleware)

  let hotMiddleware = require('webpack-hot-middleware')(compiler)

  // force page reload when html-webpack-plugin template changes
  compiler.plugin('compilation', compilation => {
    compilation.plugin('html-webpack-plugin-after-emit', (data, cb) => {
      hotMiddleware.publish({action: 'reload'})
      cb()
    })
  })

  app.use(hotMiddleware)

  // serve pure static assets
  app.use('/', express.static('./static'))

  /*
   * app.get("/", (req, res) => {
   *  res.sendFile(__dirname + '/index.html')
   * })
  */
  return app
}

const ENDPOINT = 'http://parish.host/parse'
const APP_ID = 'parish-connect'

Parse.initialize(APP_ID, 'u6JT3oU332', 'u6JT3oU332')
Parse.serverURL = ENDPOINT
Parse.Cloud.useMasterKey()

// module.exports = Parse.Cloud.run('getData', { pAccountId: '7LxJRkKgeu'})
module.exports = Parse.Cloud.run('getData')
  .then(data => {
    return createDevServer(data).listen(port, err => {
      if (err) {
        console.log('Error: ', err)
        return
      }
      let uri = 'http://localhost:' + port
      console.log('Listening at ' + uri + '\n')
    })
  })
  .catch(err => { console.error(err) })
