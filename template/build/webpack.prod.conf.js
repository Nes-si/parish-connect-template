const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')

const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const projectRoot = path.resolve(__dirname, '../')
const utils = require('./utils')

const makeBaseConfig = require('./webpack.base.conf')

// Why?!
process.env.NODE_ENV = 'production'

const makeConfig = () => {
  const baseWebpackConfig = makeBaseConfig()

  return merge(baseWebpackConfig, {
    module: {
      loaders: utils.styleLoaders({ sourceMap: true, extract: true })
    },
    output: {
      path: path.resolve(__dirname, '../dist'),
      filename: 'js/[name].js',
      chunkFilename: 'js/[id].js'
    },
    vue: {
      loaders: utils.cssLoaders({
        sourceMap: false,
        extract: true
      })
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.join(projectRoot, '/src/index.pug'),
        inject: true,
        filename: 'index.html',
        chunksSortMode: 'dependency',
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {warnings: false}
      }),
      new ExtractTextPlugin('css/[name].[contenthash].css'),
      // split vendor js into its own file
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: (module, count) => {
          // any required modules inside node_modules are extracted to vendor
          return (
            module.resource &&
            /\.js$/.test(module.resource) &&
            module.resource.indexOf(
              path.join(__dirname, '../node_modules')
            ) === 0
          )
        }
      }),
      // extract webpack runtime and module manifest to its own file in order to
      // prevent vendor hash from being updated whenever app bundle is updated
      new webpack.optimize.CommonsChunkPlugin({
        name: 'manifest',
        chunks: ['vendor']
      })
    ]
  })
}
  // let CompressionWebpackPlugin = require('compression-webpack-plugin')

  // webpackConfig.plugins.push(
  //   new CompressionWebpackPlugin({
  //     asset: '[path].gz[query]',
  //     algorithm: 'gzip',
  //     test: /\.(js|css)$/,
  //     threshold: 10240,
  //     minRatio: 0.8
  //   })
  // )

module.exports = makeConfig
