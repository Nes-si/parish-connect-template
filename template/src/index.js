import 'babel-polyfill';
import Vue           from 'vue';
import VueRouter     from 'vue-router';
import VueI18n       from 'vue-i18n'

import App           from './App';
import Home          from 'pages/Home';
import Contact       from 'pages/Contact';
import Sacraments    from 'pages/Sacraments';
import Staff         from 'pages/Staff';
import About         from 'pages/About';
import Bulletins     from 'pages/Bulletins';
import General       from 'pages/General';
import Worship       from 'pages/Worship';
import Calendar      from 'pages/Calendar';
import Event         from 'pages/Event';
import Programs      from 'pages/Programs';
import Ministries    from 'pages/Ministries';
import Announcements from 'pages/Announcements';
import Rectory       from 'pages/Rectory';

const eng = false;

export const PAGE_HOME          = 'home';
export const PAGE_ALL           = 'all';
export const PAGE_SACRAMENTS    = 'sacraments';
export const PAGE_CONTACT       = 'contact';
export const PAGE_STAFF         = 'staff';
export const PAGE_ABOUT         = 'about';
export const PAGE_BULLETINS     = 'bulletins';
export const PAGE_GENERAL       = 'general';
export const PAGE_WORSHIP       = 'worship';
export const PAGE_CALENDAR      = 'calendar';
export const PAGE_EVENT         = 'event';
export const PAGE_MINISTRIES    = 'ministries';
export const PAGE_PROGRAMS      = 'programs';
export const PAGE_ANNOUNCEMENTS = 'announcements';
export const PAGE_RECTORY       = 'rectory';

import moment from 'moment';
import 'moment/locale/es';
console.log(DATA);


import locales from './locales/'

Vue.use(VueI18n)
Vue.config.lang = DATA.Parish.languageDefault.encodedValue
Object.keys(locales).forEach((lang) => Vue.locale(lang, locales[lang]))

if (DATA.Parish.languageDefault.encodedValue === 'es')
  moment.locale('es')

// plugin for check DATA items on existance
const ObjectNotEmptyPlugin = {
  install (Vue, options) {
    Vue.prototype.$objectNotEmpty = (obj) => obj && Object.keys(obj).length > 0
  }
}
Vue.use(ObjectNotEmptyPlugin)

Vue.use(VueRouter);

export const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      name: PAGE_HOME,
      path: eng ? '/' : "/",
      component: Home
    },
    {
      name: PAGE_SACRAMENTS,
      path: eng ? '/sacraments' : "/sacramentos",
      component: Sacraments
    },
    {
      name: PAGE_CONTACT,
      path: eng ? '/contact' : "/contacto",
      component: Contact
    },
    {
      name: PAGE_STAFF,
      path: eng ? '/staff' : "/personal",
      component: Staff
    },
    {
      name: PAGE_ABOUT,
      path: eng ? '/about' : "/acerca-de",
      component: About
    },
    {
      name: PAGE_BULLETINS,
      path: eng ? '/bulletins' : "/boletines",
      component: Bulletins
    },
    {
      name: PAGE_GENERAL,
      path: eng ? '/general' : "/general",
      component: General
    },
    {
      name: PAGE_WORSHIP,
      path: eng ? '/worship' : "/horarios",
      component: Worship
    },
    {
      name: PAGE_CALENDAR,
      path: eng ? '/calendar' : "/calendario",
      component: Calendar
    },
    {
      name: PAGE_EVENT,
      path: eng ? '/calendar/events/:index' : "/calendario/eventos/:index",
      component: Event
    },
    {
      name: PAGE_PROGRAMS,
      path: eng ? '/programs' : "/programas",
      component: Programs
    },
    {
      name: PAGE_MINISTRIES,
      path: eng ? '/ministries' : "/ministerios",
      component: Ministries
    },
    {
      name: PAGE_ANNOUNCEMENTS,
      path: eng ? '/announcements' : "/anuncios",
      component: Announcements
    },
    {
      name: PAGE_RECTORY,
      path: eng ? '/rectory' : "/casa-del-parroco",
      component: Rectory
    },
    {
      name: PAGE_ALL,
      path: '*',
      component: Home
    },
  ]
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
