export default {
  daysOfWeek: {
    short: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    large: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
  },
  components: {
    announcements: {
      announcementTitle: 'ANUNCIO PARROQUIAL',
      announcementButton: 'Lee más'
    },
    events: {
      eventsHeader: 'Listado de eventos',
      eventsMore: 'Más detalles',
      eventsNone: 'No events at this day'
    },
    footer: {
      privacyPolicy: 'Privacy Policy',
      termsOfUse: 'Terms of Use',
      contact: 'Contact',
      footerCopy: 'Copyright 2017 Angelus News'
    },
    header: {
      navMobileHeading: 'MENU',
      followText: 'Síguenos en'
    },
    subnav: {

    }
  },
  pages: {
    about: {

    },
    announcements: {

    },
    bulletins: {
      latestHeader: 'Últimos Boletines'
    },
    calendar: {

    },
    contact: {
      form: {
        contactHeading: 'Contacto Inmediato',
        formSubtitle: 'Para contactarnos rápidamente, por favor utilice el formulario de contacto a continuación y nos pondremos en contacto con usted lo antes posible!',
        formInner: {
          name: 'Nombres',
          phoneNumber: 'Número de teléfono',
          email: 'E-mail*',
          webSite: 'Sitio web',
          message: 'Your Message*',
          submit: 'Enviar'
        }
      },
      locations: {
        contactHeading: 'Ubicación',
        mainParishBuilding: 'Edificio Parroquial Principal',
        rectory: 'Secretaría Parroquial',
        locationAddress: 'Dirección',
        locationContact: 'Contacto',
        locationButton: '¿Como llegar?'
      }
    },
    event: {
      backButton: 'Volver',
      backLabel: 'Volver a calendario'

    },
    general: {

    },
    home: {
      massTimes: 'MASS TIMES',
      confessionTimes: 'CONFESIÓN TIMES',
      otherTimes: 'OTHER TIMES',

      newsHeader: 'Noticias',
      newsMobileHeaderIcon: 'Noticias',
      articlesHeader: 'FORMACIÓN',
      articlesLink: 'Artículo completo >>',
      articlesMore: 'Más formación >>',
      bulletinsHeader: 'ÚLTIMOS BOLETINES',
      bulletinsDownload: 'Descargar',
      bulletinsMore: 'Más Boletines >>',
      eventsHeader: 'Calendario Parroquial',
      eventsMore: 'Más >>',

      worshipMore: 'Más >>',

      aboutLabel: 'Quiénes somos',
      aboutMore: 'Leer más >>'


    },
    ministries: {

    },
    programs: {

    },
    rectory: {

    },
    sacraments: {
      sacramentsNavHeader: 'Sacrament | Sacramentos'
    },
    staff: {
      staffHeading: 'Directorio de los miembros del personal'
    },
    worship: {
      header: 'Horarios',
      massTimes: 'MASS TIMES',
      confessionTimes: 'CONFESIÓN TIMES',
      otherTimes: 'OTHER TIMES',

      scheduleTitle: 'Código',
      scheduleItem: 'Horarios de misa',
      scheduleItemConfession: 'Confesións',
      scheduleItemDevotions: 'Devociones'
    }
  }
}
