export default {
  daysOfWeek: {
    short: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    large: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  },
  components: {
    announcements: {
      announcementTitle: 'ANNOUNCEMENT OF THE PARISH',
      announcementButton: 'Read more'
    },
    events: {
      eventsHeader: 'List of Events',
      eventsMore: 'More Details',
      eventsNone: 'No events at this day'
    },
    footer: {
      privacyPolicy: 'Privacy Policy',
      termsOfUse: 'Terms of Use',
      contact: 'Contact',
      footerCopy: '© 2016 The Roman Catholic Archbishop of Los Angeles, a corp. sole. All rights reserved.'
    },
    header: {
      navMobileHeading: 'MENU',
      followText: 'Follow Us'
    },
    subnav: {

    }
  },
  pages: {
    about: {

    },
    announcements: {

    },
    bulletins: {
      latestHeader: 'Latest Bulletins'
    },
    calendar: {

    },
    contact: {
      form: {
        contactHeading: 'Quick Contact',
        formSubtitle: 'To quickly reach us, please use the contact form below and we will get back to you as soon as possible!',
        formInner: {
          name: 'Full Name',
          phoneNumber: 'Phone number',
          email: 'E-mail*',
          webSite: 'Website',
          message: 'Your Message*',
          submit: 'Submit'
        }
      },
      locations: {
        contactHeading: 'Location',
        mainParishBuilding: 'Main Parish Building',
        rectory: 'Rectory',
        locationAddress: 'Address',
        locationContact: 'Contact',
        locationButton: 'Directions'
      }
    },
    event: {
      backButton: 'Volver',
      backLabel: 'Volver a calendario'

    },
    general: {

    },
    home: {
      massTimes: 'MASS TIMES',
      confessionTimes: 'CONFESSION TIMES',
      otherTimes: 'OTHER TIMES',

      newsHeader: 'News',
      newsMobileHeaderIcon: 'Angelus News',
      articlesHeader: 'education',
      articlesLink: 'Read article >>',
      articlesMore: 'More information >>',
      bulletinsHeader: 'Latest Bulletins',
      bulletinsDownload: 'Download',
      bulletinsMore: 'More Bulletins >>',
      eventsHeader: 'Parish Calendar',
      eventsMore: 'More >>',

      worshipMore: 'More >>',

      aboutLabel: 'About us',
      aboutMore: 'Read more >>'


    },
    ministries: {

    },
    programs: {

    },
    rectory: {

    },
    sacraments: {
      sacramentsNavHeader: 'Sacrament | Sacraments'
    },
    staff: {
      staffHeading: 'Directory of Staff Members'
    },
    worship: {
      header: 'Worship Schedule',
      massTimes: 'MASS TIMES',
      confessionTimes: 'CONFESSION TIMES',
      otherTimes: 'OTHER TIMES',

      scheduleTitle: 'Schedule Key',
      scheduleItem: 'Mass Times',
      scheduleItemConfession: 'Confessions',
      scheduleItemDevotions: 'Devotions'
    }
  }
}
