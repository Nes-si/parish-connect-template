const build = require('../template/build/build.js');
const initModels = require('./initModels.js');
const migrateModels = require('./migrateModels.js');
const getData = require('./getData.js');
const iniateBuild = require('../build');


Parse.Cloud.define('hello', (req, res) => {
  build({data: 'test'})
    .then(status => res.success(status))
    .catch(err => res.error(err));
});

Parse.Cloud.define('initModels', initModels);
Parse.Cloud.define('migrateModels', migrateModels);
Parse.Cloud.define('getData', (req, res) =>
  getData(req)
  .then(res.success)
  .catch(res.error)
);

Parse.Cloud.define('localbuild', (req, res) => {

  console.log('build: ', req.params)

  getData(req)
  .then((data) => {
    return build(data)
  })
  .then(() => {
    res.success('built')
  })
  .catch(e => {
    console.log('::ERR: ', e);
    res.error(e)
  });
})

Parse.Cloud.define('deploy', (req, res) => {

  console.log('deploy: ', req.params)

  getData(req)
  .then((data) => {
    return iniateBuild({
      domain: req.params.domain || 'pepe.parish.host',
      data: data
    })
  })
  .then(() => {
    res.success('deployed')
  })
  .catch(e => {
    console.log('::ERR: ', e);
    res.error(e)
  });
});


Parse.Cloud.define('checkRegCode', (req, res) => {
  if (req.params.code == 'IDDQD')
    res.success("Right code!");
  else
    res.error("Wrong code!");
});
