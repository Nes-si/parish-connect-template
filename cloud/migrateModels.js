const modelsStructure = require('./data.js');

module.exports = (req, res) => {
  let prWr = obj => {
    return new Promise((resolve, reject) => {
      obj.then(resolve, reject);
    });
  };
  let ParishAccount = Parse.Object.extend('ParishAccount');
  let queryTest = new Parse.Query(ParishAccount);

  let NewsArticle = Parse.Object.extend('NewsArticle');
  let newsArticle = new NewsArticle();

  let ResourcesArticle = Parse.Object.extend('ResourcesArticle');
  let resourcesArticle = new ResourcesArticle();

  queryTest.first()
    .then(parishAccount => {

      //=========== with Parish account relation
      newsArticle.set('title', 'Good news!');
      newsArticle.set('image', null);
      newsArticle.set('diocese', 'Catholic LA');
      newsArticle.set('body', '<p>Some article text</p>');
      newsArticle.set('date', new Date(2016, 11, 11));
      newsArticle.set('account', parishAccount);

      resourcesArticle.set('title', 'Useful article!');
      resourcesArticle.set('image', null);
      resourcesArticle.set('diocese', 'Education');
      resourcesArticle.set('body', '<p>Some article text</p>');
      resourcesArticle.set('date', new Date(2016, 11, 11));
      resourcesArticle.set('account', parishAccount);

    })

    .then(() => prWr(newsArticle.save()))
    .then(() => prWr(resourcesArticle.save()))

    .then(() => res.success({result: 'Migration completed!'}))
    .catch((e) => res.error(e));;

}
