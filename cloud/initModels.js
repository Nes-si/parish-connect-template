const modelsStructure = require('./data.js');

module.exports = (req, res) => {
  let prWr = obj => {
    return new Promise((resolve, reject) => {
      obj.then(resolve, reject);
    });
  };
  let queryTest = new Parse.Query(Parse.User);
  queryTest.first()
    .then(result => {
      if (result) {
        res.success({result: 'Already initialized!', modelsStructure});
        return;
      }
      let user = new Parse.User();
      user.set("username", 'admin@example.com');
      user.set("email", 'admin@example.com');
      user.set("password", 'admin');

      //==== without Parish account relation

      let ParishConnectLanguage = Parse.Object.extend('ParishConnectLanguage');
      let parishConnectLanguages = [];

      let languageEn = new ParishConnectLanguage();
      languageEn.set('name', 'English');
      languageEn.set('encodedValue', 'en');
      languageEn.set('image', null);
      parishConnectLanguages.push(languageEn);

      let languageEs = new ParishConnectLanguage();
      languageEs.set('name', 'Spanish');
      languageEs.set('encodedValue', 'es');
      languageEs.set('image', null);
      parishConnectLanguages.push(languageEs);


      let Title = Parse.Object.extend('Title');
      let titles = [];

      let title = new Title();
      title.set('language', languageEn);
      title.set('name', 'Father');
      titles.push(title);

      title = new Title();
      title.set('language', languageEn);
      title.set('name', 'Pastor');
      titles.push(title);

      title = new Title();
      title.set('language', languageEn);
      title.set('name', 'Minister');
      titles.push(title);


      let Position = Parse.Object.extend('Position');
      let positions = [];

      let position = new Position();
      position.set('language', languageEn);
      position.set('name', 'accountant');
      positions.push(position);

      position = new Position();
      position.set('language', languageEn);
      position.set('name', 'precentor');
      positions.push(position);


      let MassType = Parse.Object.extend('MassType');
      let massTypes = [];

      let massType = new MassType();
      massType.set('language', languageEn);
      massType.set('name', 'Ordinary Mass');
      massTypes.push(massType);

      massType = new MassType();
      massType.set('language', languageEn);
      massType.set('name', 'Daily Mass');
      massTypes.push(massType);

      massType = new MassType();
      massType.set('language', languageEn);
      massType.set('name', 'Feast Day');
      massTypes.push(massType);


      let OtherTimeType = Parse.Object.extend('OtherTimeType');
      let otherTimeTypes = [];

      let otherTimeType = new OtherTimeType();
      otherTimeType.set('language', languageEn);
      otherTimeType.set('name', 'Other time 1');
      otherTimeTypes.push(otherTimeType);

      otherTimeType = new OtherTimeType();
      otherTimeType.set('language', languageEn);
      otherTimeType.set('name', 'Other time 2');
      otherTimeTypes.push(otherTimeType);


      let Menu = Parse.Object.extend('Menu');
      let menus = [];

      let menu = new Menu();
      menu.set('language', languageEn);
      menu.set('itemHome',      'Home');
      menu.set('itemBulletins', 'Bulletins');
      menu.set('itemParish',    'Parish');
      menu.set('itemWorship',   'Worship');
      menu.set('itemCalendar',  'Calendar');
      menu.set('itemContact',   'Contact');
      menu.set('itemAbout',     'About our parish');
      menu.set('itemMinistries','Ministries');
      menu.set('itemPrograms',  'Programs');
      menu.set('itemRectory',   'Rectory');
      menu.set('itemSacraments','Sacraments');
      menu.set('itemStaff',     'Staff directory');
      menus.push(menu);

      menu = new Menu();
      menu.set('language', languageEs);
      menu.set('itemHome',      'Inicio');
      menu.set('itemBulletins', 'Boletines');
      menu.set('itemParish',    'Parroquia');
      menu.set('itemWorship',   'Adoración');
      menu.set('itemCalendar',  'Calendario');
      menu.set('itemContact',   'Contacto');
      menu.set('itemAbout',     'Acerca de nuestra parroquia');
      menu.set('itemMinistries','Ministerios');
      menu.set('itemPrograms',  'Programas');
      menu.set('itemRectory',   'Casa del párroco');
      menu.set('itemSacraments','Sacramentos');
      menu.set('itemStaff',     'Directorio de Personal');
      menus.push(menu);


     //here

     let LinkMenu = Parse.Object.extend('LinkMenu');
     let linkMenu = new LinkMenu();
     linkMenu.set('name', 'Angelus News');
     linkMenu.set('URL', 'http://angelusnews.com/');
     linkMenu.set('account', parishAccount);


      //========= Parish account

      let ParishAccount = Parse.Object.extend('ParishAccount');
      let parishAccount = new ParishAccount();
      parishAccount.set('owner', user);
      parishAccount.set('colorMain', '#ff0000');
      parishAccount.set('name', 'example.com');
      parishAccount.set('domainURL', 'http://example.com');
      parishAccount.set('approved', true);


      //=========== with Parish account relation

      let Parish = Parse.Object.extend('Parish');
      let parish = new Parish();
      parish.set('name', 'Example Parish');
      parish.set('street', 'Example St.');
      parish.set('city', 'New-York');
      parish.set('state', 'California');
      parish.set('country', 'USA');
      parish.set('zip', '123456');
      parish.set('phonePrimary', '0987654321');
      parish.set('email', 'example@examp.le');
      parish.set('languageDefault', languageEn);
      parish.set('imageLogo', null);
      parish.set('facebook', 'http://www.facebook.com');
      parish.set('twitter', 'http://www.twitter.com');
      parish.set('instagram', 'http://instagram.com');
      parish.set('youtube', 'http://youtube.com');
      parish.set('imageMission', null);
      parish.set('imageAbout', null);
      parish.set('domain', 'http://www.reg.ru');
      parish.set('account', parishAccount);

      let About = Parse.Object.extend('About');
      let about = new About();
      about.set('language', languageEn);
      about.set('description', 'We are amazing!');
      about.set('detail', "<p>We are amazing! Let's come together!</p>");
      about.set('account', parishAccount);

      let MissionStatement = Parse.Object.extend('MissionStatement');
      let missionStatement = new MissionStatement();
      missionStatement.set('language', languageEn);
      missionStatement.set('detail', 'We are wonderful!');
      missionStatement.set('account', parishAccount);

      let ParishAdministration = Parse.Object.extend('ParishAdministration');
      let parishAdministration = new ParishAdministration();
      parishAdministration.set('position', position);
      parishAdministration.set('title', title);
      parishAdministration.set('nameFirst', 'John');
      parishAdministration.set('nameLast', 'Doe');
      parishAdministration.set('image', null);
      parishAdministration.set('account', parishAccount);

      let ParishAdminBio = Parse.Object.extend('ParishAdminBio');
      let parishAdminBio = new ParishAdminBio();
      parishAdminBio.set('language', languageEn);
      parishAdminBio.set('bio', 'I am the best!');
      parishAdminBio.set('account', parishAccount);

      let SliderMedia = Parse.Object.extend('SliderMedia');
      let sliderMedia = new SliderMedia();
      sliderMedia.set('position', 1);
      sliderMedia.set('image', null);
      sliderMedia.set('account', parishAccount);

      let MassTime = Parse.Object.extend('MassTime');
      let massTime = new MassTime();
      massTime.set('massType', massType);
      massTime.set('timeStart', new Date(2000, 1, 1, 11, 0));
      massTime.set('timeEnd', new Date(2000, 1, 1, 12, 0));
      massTime.set('language', languageEn);
      massTime.set('sunday', false);
      massTime.set('monday', true);
      massTime.set('tuesday', false);
      massTime.set('wednesday', true);
      massTime.set('thursday', false);
      massTime.set('friday', false);
      massTime.set('saturday', true);
      massTime.set('account', parishAccount);

      let ConfessionTime = Parse.Object.extend('ConfessionTime');
      let confessionTime = new ConfessionTime();
      confessionTime.set('timeStart', new Date(2000, 1, 1, 16, 0));
      confessionTime.set('timeEnd', new Date(2000, 1, 1, 17, 0));
      confessionTime.set('language', languageEn);
      confessionTime.set('sunday', false);
      confessionTime.set('monday', true);
      confessionTime.set('tuesday', false);
      confessionTime.set('wednesday', false);
      confessionTime.set('thursday', false);
      confessionTime.set('friday', true);
      confessionTime.set('saturday', true);
      confessionTime.set('account', parishAccount);

      let OtherTime = Parse.Object.extend('OtherTime');
      let otherTime = new OtherTime();
      otherTime.set('type', otherTimeType);
      otherTime.set('timeStart', new Date(2000, 1, 1, 10, 0));
      otherTime.set('timeEnd', new Date(2000, 1, 1, 11, 0));
      otherTime.set('language', languageEn);
      otherTime.set('sunday', false);
      otherTime.set('monday', false);
      otherTime.set('tuesday', false);
      otherTime.set('wednesday', false);
      otherTime.set('thursday', false);
      otherTime.set('friday', false);
      otherTime.set('saturday', true);
      otherTime.set('account', parishAccount);

      let Sacrament = Parse.Object.extend('Sacrament');
      let sacrament = new Sacrament();
      sacrament.set('language', languageEn);
      sacrament.set('name', 'Sacrament1');
      sacrament.set('detail', '<p>Sacrament is sacramental</p>');
      sacrament.set('account', parishAccount);

      let Ministry = Parse.Object.extend('Ministry');
      let ministry = new Ministry();
      ministry.set('language', languageEn);
      ministry.set('name', 'Ministry1');
      ministry.set('detail', '<p>Ministry ministry ministry ministry</p>');
      ministry.set('account', parishAccount);

      let OtherProgram = Parse.Object.extend('OtherProgram');
      let otherProgram = new OtherProgram();
      otherProgram.set('language', languageEn);
      otherProgram.set('name', 'Other Program 1');
      otherProgram.set('detail', '<p>We are not such as all</p>');
      otherProgram.set('account', parishAccount);

      let Rectory = Parse.Object.extend('Rectory');
      let rectory = new Rectory();
      rectory.set('title', title);
      rectory.set('nameFirst', 'Jane');
      rectory.set('nameLast', 'Smith');
      rectory.set('street', 'First str.');
      rectory.set('city', 'Los-Angeles');
      rectory.set('zip', '344000');
      rectory.set('state', 'Washington');
      rectory.set('country', 'USA');
      rectory.set('phone', '04874939');
      rectory.set('email', 'rectory@examp.le');
      rectory.set('account', parishAccount);

      let Staff = Parse.Object.extend('Staff');
      let staff = new Staff();
      staff.set('position', position);
      staff.set('nameFirst', 'Josh');
      staff.set('nameLast', 'Brown');
      staff.set('photo', null);
      staff.set('phone', '04874939');
      staff.set('email', 'staff@examp.le');
      staff.set('account', parishAccount);

      let School = Parse.Object.extend('School');
      let school = new School();
      school.set('type', 'HIGH');
      school.set('name', 'St. School');
      school.set('title', title);
      school.set('contactNameFirst', 'Joseph');
      school.set('contactNameLast', 'Green');
      school.set('street', 'First str.');
      school.set('city', 'Los-Angeles');
      school.set('ZIP', '344000');
      school.set('state', 'Washington');
      school.set('country', 'USA');
      school.set('phone', '04874939');
      school.set('email', 'rectory@examp.le');
      school.set('account', parishAccount);

      let SupportedLanguage = Parse.Object.extend('SupportedLanguage');
      let supportedLanguage = new SupportedLanguage();
      supportedLanguage.set('language', languageEn);
      supportedLanguage.set('completed', true);
      supportedLanguage.set('waitingReview', false);
      supportedLanguage.set('approved', true);
      supportedLanguage.set('account', parishAccount);

      let Event_type = Parse.Object.extend('Event');
      let event = new Event_type();
      event.set('name', 'Meetup');
      event.set('place', 'Cathedral');
      event.set('description', 'JS Meetup');
      event.set('day', new Date(2016, 12, 12));
      event.set('timeStart', new Date(2000, 1, 1, 10, 0));
      event.set('timeEnd', new Date(2000, 1, 1, 11, 0));
      event.set('account', parishAccount);

      let Bulletin = Parse.Object.extend('Bulletin');
      let bulletin = new Bulletin();
      bulletin.set('name', 'Meetup');
      bulletin.set('file', null);
      bulletin.set('account', parishAccount);

      let Announcement = Parse.Object.extend('Announcement');
      let announcement = new Announcement();
      announcement.set('title', 'Meetup');
      announcement.set('name', 'Meetup is here');
      announcement.set('description', 'JS Meetup');
      announcement.set('content', '<p>JS Meetup is here!</p>');
      announcement.set('date', new Date(2016, 11, 11));
      announcement.set('account', parishAccount);


      let NewsArticle = Parse.Object.extend('NewsArticle');
      let newsArticle = new NewsArticle();
      newsArticle.set('title', 'Good news!');
      newsArticle.set('image', null);
      newsArticle.set('diocese', 'Catholic LA');
      newsArticle.set('body', '<p>Some article text</p>');
      newsArticle.set('date', new Date(2016, 11, 11));
      newsArticle.set('account', parishAccount);

      let ResourceArticle = Parse.Object.extend('ResourceArticle');
      let resourceArticle = new ResourceArticle();
      resourceArticle.set('title', 'Useful article!');
      resourceArticle.set('image', null);
      resourceArticle.set('diocese', 'Education');
      resourceArticle.set('body', '<p>Some article text</p>');
      resourceArticle.set('date', new Date(2016, 11, 11));
      resourceArticle.set('account', parishAccount);



      prWr(user.signUp())
        .then(() => Promise.all(parishConnectLanguages.map(elm => prWr(elm.save()))))
        .then(() => Promise.all(titles.map(elm => prWr(elm.save()))))
        .then(() => Promise.all(positions.map(elm => prWr(elm.save()))))
        .then(() => Promise.all(massTypes.map(elm => prWr(elm.save()))))
        .then(() => Promise.all(otherTimeTypes.map(elm => prWr(elm.save()))))
        .then(() => Promise.all(menus.map(elm => prWr(elm.save()))))


        .then(() => prWr(parishAccount.save()))

        .then(() => prWr(parish.save()))
        .then(() => prWr(about.save()))
        .then(() => prWr(missionStatement.save()))
        .then(() => prWr(parishAdministration.save()))
        .then(() => prWr(parishAdminBio.save()))
        .then(() => prWr(sliderMedia.save()))
        .then(() => prWr(massTime.save()))
        .then(() => prWr(confessionTime.save()))
        .then(() => prWr(otherTime.save()))
        .then(() => prWr(sacrament.save()))
        .then(() => prWr(ministry.save()))
        .then(() => prWr(otherProgram.save()))
        .then(() => prWr(rectory.save()))
        .then(() => prWr(staff.save()))
        .then(() => prWr(school.save()))
        .then(() => prWr(supportedLanguage.save()))
        .then(() => prWr(event.save()))
        .then(() => prWr(bulletin.save()))
        .then(() => prWr(announcement.save()))
        .then(() => prWr(newsArticle.save()))
        .then(() => prWr(resourceArticle.save()))
        .then(() => prWr(linkMenu.save()))

        .then(() => res.success({result: 'Initialization completed!', modelsStructure}))
        .catch((e) => res.error(e));

    }, e => res.error(e));

}
