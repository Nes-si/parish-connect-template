const modelsStructure = require('./data.js');

const modelNames = [
  'Parish',
  'About',
  'MissionStatement',
  'ParishAdministration',
  'ParishAdminBio',
  'Rectory',
  'School',
  ['SliderMedia'],
  ['MassTime'],
  ['ConfessionTime'],
  ['NewsArticle'],
  ['ResourcesArticle'],
  ['OtherTime'],
  ['Sacrament'],
  ['Ministry'],
  ['OtherProgram'],
  ['Staff'],
  ['Event'],
  ['Bulletin'],
  ['Announcement'],
  ['LinksMenu'],
  ['Menu'],
];

const getFields = className => {
  return modelsStructure
    .filter(m => (m.nameId === className))[0].fields;
}


const getReferences = (className, parent='') => {
  return getFields(className)
  .filter(f => (f.type === 'Reference' || f.type === 'File'))
  .reduce((arr, ref) => {
    const relativeName = parent ? `${parent}.${ref.nameId}` : `${ref.nameId}`
    if (ref.type === 'File') return [...arr, relativeName]

    return [
      ...arr,
      relativeName,
      ...getReferences(ref.appearance, relativeName)
    ]


  }, [])

}


function getModel(className, site) {
  const isArray = Array.isArray(className);
  if (isArray)
    className = className[0];

  const Model = Parse.Object.extend(className);
  let modelQuery = new Parse.Query(Model);
  if (className !== "Menu")
    modelQuery.equalTo('account', site);

  getReferences(className).forEach(ref => {
    modelQuery.include(ref);
  });

  return isArray ? modelQuery.find() : modelQuery.first();
}

function collectFields(model) {
  return getFields(model.className).reduce((m, f) => {
    let val = model.get(f.nameId);

    if (!val) {
      m[f.nameId] = val;
      return m;
    }

    if(f.type=="Date/time") {
      m[f.nameId] = val.getTime();
      return m;
    }

    if (!val.className)
      m[f.nameId] = val;
    else if (val.className == 'MediaItem') {
     console.log('media: ', val);
     m[f.nameId] = {
        id: val.id,
        name: val.get('name'),
        file: val.get('file').url(),
      }
    }
    else
      m[f.nameId] = collectFields(val);

    return m;
  }, {});
}


function getData(req){
  const Site = Parse.Object.extend("ParishAccount");
  let siteQuery = new Parse.Query(Site);
  console.log('pAccountId:', req.params.pAccountId);
  if (!!req.params.pAccountId)
    siteQuery.equalTo("objectId", req.params.pAccountId);

  console.log('Getting data...')

  return new Promise((res,rej) => {
    siteQuery.first()
      .then(site => {
        console.log('Site: ', site)
        return Promise.all(modelNames.map(m => getModel(m, site)));
      })
      .then(data => {
        return data.reduce((result, model) => {
          const isArray = Array.isArray(model);

          if (!model) return result;
          if (isArray && !model[0]) return result;
          const className = isArray ? model[0].className : model.className;

          result[className] = isArray
            ? model.map(item => collectFields(item))
            : collectFields(model);
          return result;
        }, {});
      })
      .then(res)
      .catch(e => {
        console.log('ERR: ', e);
        rej(e)

      });
  })

};

module.exports = getData;
