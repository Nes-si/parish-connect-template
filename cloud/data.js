const FIELD_TYPE_SHORT_TEXT   = "Short Text";
const FIELD_TYPE_LONG_TEXT    = "Long Text";
const FIELD_TYPE_INTEGER      = "Number Int";
const FIELD_TYPE_FLOAT        = "Number";
const FIELD_TYPE_BOOLEAN      = "Boolean";
const FIELD_TYPE_DATE         = "Date/time";
const FIELD_TYPE_FILE         = "File";
const FIELD_TYPE_REFERENCE    = "Reference";
const FIELD_TYPE_COLOR        = "Color";
const FIELD_TYPE_ENUMERATION  = "Enumeration";


const FIELD_APPEARANCE__SHORT_TEXT__SINGLE  = "Single line";
const FIELD_APPEARANCE__SHORT_TEXT__URL     = "URL";
const FIELD_APPEARANCE__SHORT_TEXT__EMAIL   = "Email";

const FIELD_APPEARANCE__LONG_TEXT__MULTI    = "Multi-line";
const FIELD_APPEARANCE__LONG_TEXT__WYSIWIG  = "WYSIWYG";

const FIELD_APPEARANCE__INTEGER__DECIMAL    = "Decimal";
const FIELD_APPEARANCE__INTEGER__RATING     = "Rating";

const FIELD_APPEARANCE__FLOAT__DECIMAL      = "Decimal";

const FIELD_APPEARANCE__BOOLEAN__RADIO      = "Radio buttons";
const FIELD_APPEARANCE__BOOLEAN__SWITCH     = "Switch";

const FIELD_APPEARANCE__DATE__DATE_TIME     = "Date & time";
const FIELD_APPEARANCE__DATE__DATE          = "Date";
const FIELD_APPEARANCE__DATE__TIME          = "Time";

const FIELD_APPEARANCE__FILE__IMAGE         = "Image";
const FIELD_APPEARANCE__FILE__PDF           = "PDF";

const FIELD_APPEARANCE__COLOR__COLOR        = "Color";


const modelsStructure = [
  {
    name: 'Parish Connect language',
    nameId: 'ParishConnectLanguage',
    description: 'ParishConnectLanguage',
    color: '#82fac7',
    common: true,
    fields: [
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Encoded value',
        nameId: 'encodedValue',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Image',
        nameId: 'image',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      }
    ]
  },
  {
    name: 'Title',
    nameId: 'Title',
    description: 'Title',
    color: '#124906',
    common: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      }
    ]
  },
  {
    name: 'Position',
    nameId: 'Position',
    description: 'Position',
    color: '#901895',
    common: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      }
    ]
  },
  {
    name: 'Mass type',
    nameId: 'MassType',
    description: 'Mass Type',
    color: '#196280',
    common: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      }
    ]
  },
  {
    name: 'Other time type',
    nameId: 'OtherTimeType',
    description: 'Other Time Type',
    color: '#799519',
    common: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      }
    ]
  },

  {
    name: 'Menu',
    nameId: 'Menu',
    description: 'Menu items',
    color: '#8e3143',
    common: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Item: Home',
        nameId: 'itemHome',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Bulletins',
        nameId: 'itemBulletins',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Parish',
        nameId: 'itemParish',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Worship',
        nameId: 'itemWorship',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Calendar',
        nameId: 'itemCalendar',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Contact',
        nameId: 'itemContact',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: About our parish',
        nameId: 'itemAbout',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Ministries',
        nameId: 'itemMinistries',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Programs',
        nameId: 'itemPrograms',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Rectory',
        nameId: 'itemRectory',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Sacraments',
        nameId: 'itemSacraments',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Item: Staff directory',
        nameId: 'itemStaff',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      }
    ]
  },

  {
    name: 'Parish',
    nameId: 'Parish',
    description: 'Parish',
    color: '#1ac7f2',
    single: true,
    fields: [
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Street',
        nameId: 'street',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'City',
        nameId: 'city',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'State',
        nameId: 'state',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Country',
        nameId: 'country',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'ZIP',
        nameId: 'zip',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Primary phone',
        nameId: 'phonePrimary',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Email',
        nameId: 'email',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__EMAIL
      },
      {
        name: 'Default language',
        nameId: 'languageDefault',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Logo image',
        nameId: 'imageLogo',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      },
      {
        name: 'Facebook',
        nameId: 'facebook',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__URL
      },
      {
        name: 'Twitter',
        nameId: 'twitter',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__URL
      },
      {
        name: 'Instagram',
        nameId: 'instagram',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__URL
      },
      {
        name: 'Youtube',
        nameId: 'youtube',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__URL
      },
      {
        name: 'Mission image',
        nameId: 'imageMission',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      },
      {
        name: 'About image',
        nameId: 'imageAbout',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      },
      {
        name: 'Domain',
        nameId: 'domain',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__URL
      }
    ]
  },
  {
    name: 'About',
    nameId: 'About',
    description: 'About',
    color: '#748871',
    single: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Description',
        nameId: 'description',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      },
      {
        name: 'Detail',
        nameId: 'detail',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      }
    ]
  },
  {
    name: 'Mission statement',
    nameId: 'MissionStatement',
    description: 'MissionStatement',
    color: '#868018',
    single: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Detail',
        nameId: 'detail',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__MULTI
      }
    ]
  },
  {
    name: 'Parish administration',
    nameId: 'ParishAdministration',
    description: 'ParishAdministration',
    color: '#527015',
    single: true,
    fields: [
      {
        name: 'Position',
        nameId: 'position',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'Position'
      },
      {
        name: 'Title',
        nameId: 'title',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'Title'
      },
      {
        name: 'First name',
        nameId: 'nameFirst',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Last name',
        nameId: 'nameLast',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Image',
        nameId: 'image',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      }
    ]
  },
  {
    name: 'Parish Admin Bio',
    nameId: 'ParishAdminBio',
    description: 'ParishAdminBio',
    color: '#802422',
    single: true,
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Bio',
        nameId: 'bio',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      }
    ]
  },
  {
    name: 'Slider media',
    nameId: 'SliderMedia',
    description: 'MissionStatement',
    color: '#739183',
    fields: [
      {
        name: 'Position',
        nameId: 'position',
        type: FIELD_TYPE_INTEGER,
        appearance: FIELD_APPEARANCE__INTEGER__DECIMAL
      },
      {
        name: 'Image',
        nameId: 'image',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      }
    ]
  },
  {
    name: 'Mass time',
    nameId: 'MassTime',
    description: 'Mass Time',
    color: '#995898',
    fields: [
      {
        name: 'Mass type',
        nameId: 'massType',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'MassType'
      },
      {
        name: 'Start time',
        nameId: 'timeStart',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'End time',
        nameId: 'timeEnd',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Sunday',
        nameId: 'sunday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Monday',
        nameId: 'monday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Tuesday',
        nameId: 'tuesday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Wednesday',
        nameId: 'wednesday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Thursday',
        nameId: 'thursday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Friday',
        nameId: 'friday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Saturday',
        nameId: 'saturday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
    ]
  },
  {
    name: 'Confession time',
    nameId: 'ConfessionTime',
    description: 'Confession Time',
    color: '#166399',
    fields: [
      {
        name: 'Start time',
        nameId: 'timeStart',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'End time',
        nameId: 'timeEnd',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Sunday',
        nameId: 'sunday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Monday',
        nameId: 'monday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Tuesday',
        nameId: 'tuesday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Wednesday',
        nameId: 'wednesday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Thursday',
        nameId: 'thursday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Friday',
        nameId: 'friday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Saturday',
        nameId: 'saturday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
    ]
  },
  {
    name: 'Other time',
    nameId: 'OtherTime',
    description: 'Other Time',
    color: '#129590',
    fields: [
      {
        name: 'Type',
        nameId: 'type',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'OtherTimeType'
      },
      {
        name: 'Start time',
        nameId: 'timeStart',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'End time',
        nameId: 'timeEnd',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Sunday',
        nameId: 'sunday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Monday',
        nameId: 'monday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Tuesday',
        nameId: 'tuesday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Wednesday',
        nameId: 'wednesday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Thursday',
        nameId: 'thursday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Friday',
        nameId: 'friday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Saturday',
        nameId: 'saturday',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
    ]
  },
  {
    name: 'Sacrament',
    nameId: 'Sacrament',
    description: 'Sacrament',
    color: '#931019',
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Detail',
        nameId: 'detail',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      }
    ]
  },
  {
    name: 'Ministry',
    nameId: 'Ministry',
    description: 'Ministry',
    color: '#332172',
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Detail',
        nameId: 'detail',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      }
    ]
  },
  {
    name: 'Other program',
    nameId: 'OtherProgram',
    description: 'OtherProgram',
    color: '#699230',
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Detail',
        nameId: 'detail',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      }
    ]
  },
  {
    name: 'Rectory',
    nameId: 'Rectory',
    description: 'Rectory',
    color: '#404493',
    single: true,
    fields: [
      {
        name: 'Title',
        nameId: 'title',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'Title'
      },
      {
        name: 'First name',
        nameId: 'nameFirst',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Last name',
        nameId: 'nameLast',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Street',
        nameId: 'street',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'City',
        nameId: 'city',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'State',
        nameId: 'state',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Country',
        nameId: 'country',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'ZIP',
        nameId: 'zip',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Phone',
        nameId: 'phone',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Email',
        nameId: 'email',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__EMAIL
      }
    ]
  },
  {
    name: 'Staff',
    nameId: 'Staff',
    description: 'Staff',
    color: '#d4a8f8',
    fields: [
      {
        name: 'Position',
        nameId: 'position',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'Position'
      },
      {
        name: 'First name',
        nameId: 'nameFirst',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Last name',
        nameId: 'nameLast',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Photo',
        nameId: 'photo',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      },
      {
        name: 'Phone',
        nameId: 'phone',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Email',
        nameId: 'email',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__EMAIL
      }
    ]
  },
  {
    name: 'School',
    nameId: 'School',
    description: 'School',
    color: '#205838',
    single: true,
    fields: [
      {
        name: 'Type',
        nameId: 'type',
        type: FIELD_TYPE_ENUMERATION,
        appearance: ['LOW', 'MID', 'HIGH']
      },
      {
        name: 'Title',
        nameId: 'title',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'Title'
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Contact first name',
        nameId: 'contactNameFirst',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Contact last name',
        nameId: 'contactNameLast',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Street',
        nameId: 'street',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'City',
        nameId: 'city',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'State',
        nameId: 'state',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Country',
        nameId: 'country',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'ZIP',
        nameId: 'zip',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Phone',
        nameId: 'phone',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Email',
        nameId: 'email',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__EMAIL
      }
    ]
  },
  {
    name: 'Supported language',
    nameId: 'SupportedLanguage',
    description: 'Supported language',
    color: '#453071',
    fields: [
      {
        name: 'Language',
        nameId: 'language',
        type: FIELD_TYPE_REFERENCE,
        appearance: 'ParishConnectLanguage'
      },
      {
        name: 'Completed',
        nameId: 'completed',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Waiting review',
        nameId: 'waitingReview',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      },
      {
        name: 'Approved',
        nameId: 'approved',
        type: FIELD_TYPE_BOOLEAN,
        appearance: FIELD_APPEARANCE__BOOLEAN__SWITCH
      }
    ]
  },
  {
    name: 'Event',
    nameId: 'Event',
    description: 'Event',
    color: '#f56797',
    fields: [
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Description',
        nameId: 'description',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      },
      {
        name: 'Place',
        nameId: 'place',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Day',
        nameId: 'day',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__DATE
      },
      {
        name: 'Start time',
        nameId: 'timeStart',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      },
      {
        name: 'End time',
        nameId: 'timeEnd',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__TIME
      }
    ]
  },
  {
    name: 'Bulletin',
    nameId: 'Bulletin',
    description: 'Bulletin',
    color: '#601886',
    fields: [
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'File',
        nameId: 'file',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__PDF
      }
    ]
  },
  {
    name: 'Announcement',
    nameId: 'Announcement',
    description: 'Announcement',
    color: '#17573d',
    fields: [
      {
        name: 'Title',
        nameId: 'title',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Description',
        nameId: 'description',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__MULTI
      },
      {
        name: 'Content',
        nameId: 'content',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      },
      {
        name: 'Date',
        nameId: 'date',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__DATE
      }
    ]
  },
  {
    name: 'NewsArticle',
    nameId: 'NewsArticle',
    description: 'NewsArticle',
    color: '#17573d',
    fields: [
      {
        name: 'Title',
        nameId: 'title',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Link',
        nameId: 'link',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Body',
        nameId: 'body',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      },
      {
        name: 'Diocese',
        nameId: 'diocese',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Date',
        nameId: 'date',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__DATE
      },
      {
        name: 'Image',
        nameId: 'image',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      }
    ]
  },

  {
    name: 'ResourcesArticle',
    nameId: 'ResourcesArticle',
    description: 'ResourcesArticle',
    color: '#17573d',
    fields: [
      {
        name: 'Title',
        nameId: 'title',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
        isTitle: true
      },
      {
        name: 'Body',
        nameId: 'body',
        type: FIELD_TYPE_LONG_TEXT,
        appearance: FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
      },
      {
        name: 'Link',
        nameId: 'link',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Diocese',
        nameId: 'diocese',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'Date',
        nameId: 'date',
        type: FIELD_TYPE_DATE,
        appearance: FIELD_APPEARANCE__DATE__DATE
      },
      {
        name: 'Image',
        nameId: 'image',
        type: FIELD_TYPE_FILE,
        appearance: FIELD_APPEARANCE__FILE__IMAGE
      }
    ]
  },
  {
    name: 'Links menu',
    nameId: 'LinksMenu',
    description: 'Menu of links',
    color: '#456cb4',
    fields: [
      {
        name: 'Name',
        nameId: 'name',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__SINGLE
      },
      {
        name: 'URL',
        nameId: 'url',
        type: FIELD_TYPE_SHORT_TEXT,
        appearance: FIELD_APPEARANCE__SHORT_TEXT__URL
      }
    ]
  }
];

module.exports = modelsStructure;
