worker_processes 1;

events {
  worker_connections  1024;
}

http {
  include /etc/nginx/mime.types;
  gzip  on;
  gzip_http_version 1.1;
  gzip_comp_level 2;
  gzip_types text/plain text/css
    application/x-javascript text/xml
    application/xml application/xml+rss
    text/javascript;

  upstream s3server {
    # TODO: put it in ENV
    server parish-prod.s3-website-us-west-2.amazonaws.com;
  }

  server {
    # Listen on port 80 for all IPs associated with your machine
    listen 80;

    # Catch all other server names
    server_name _;

    # This code gets the host without www. in front and places it inside
    # the $host_without_www variable
    # If someone requests www.coolsite.com, then $host_without_www will have the value coolsite.com
    set $host_without_www $host;
    if ($host ~* www\.(.*)) {
      set $host_without_www $1;
    }

    location / {
      # This code rewrites the original request, and adds the host without www in front
      set $proxy_host 'http://parish-prod.s3-website-us-west-2.amazonaws.com';
      rewrite ^(.*)$ $proxy_host/$host_without_www$1 break;

      # The rewritten request is passed to S3
      proxy_pass http://s3server;
      proxy_set_header Host $host;
      proxy_redirect /$host/ /;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
  }
}

